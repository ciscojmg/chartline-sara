# Intensidad de Sennal y Calidad de Sennal
  - Aplicacion para monitorear la intensidad de sennal y la calidad del sennal del modulo Hologram Nova.

### Dependencias
Debe tener instalado [node.js](https://nodejs.org/en/)
- Entrar a la carpeta app `cd app/`
- Instalar dependencia `npm install` o `npm install --force`
- Ejecutar proyecto `node index.js`
- Ingresar a [localhost](http://localhost:3000/)

#### Multimedia
![sara](/uploads/f7f9f345464c25d51e4d82092265e3ae/sara.gif)


