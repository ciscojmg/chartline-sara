// server
const express = require('express');
const http = require('http');

// serial port
const SerialPort = require('serialport');
const ByteLength = require('@serialport/parser-byte-length');
const Delimiter = require('@serialport/parser-delimiter');
const inquirer = require('inquirer');

const schedule = require('node-schedule');

// socketio
const SocketIo = require('socket.io');
const { Console } = require('console');

// FireBase
const admin = require('firebase-admin');
const serviceAcount = require("../app/devices-sim-iot-firebase.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAcount),
    databaseURL: 'https://devices-sim-iot-default-rtdb.firebaseio.com/'
});

const db = admin.database();

// geolocation
var ipapi = require('ipapi.co');


// mqtt
const options = [{
     port: 8083 
}]
const mqtt = require('mqtt');
const client = mqtt.connect('tcp://xb4cdf95-internet-facing-a67c98d9ae4b13a0.elb.us-east-1.amazonaws.com', options);
client.options.username = 'ciscojmg';
client.options.password = '123456789';

const path = require('path');


const app = express();
const server = http.createServer(app);
const io = SocketIo.listen(server);

var ip = require("ip");

const date = require('date-and-time');


app.use(express.static(path.join(__dirname, 'public')));


// routes
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
    console.log('New Client.')
});

SerialPort.list().then(function (ports) {
    let serialPortChoices = {};
    serialPortChoices.type = 'list';
    serialPortChoices.message = 'Choose serial port you would like to connect:'
    serialPortChoices.name = "serialport";

    let listOfdevices = [];
    ports.forEach(function (port) {
        listOfdevices.push(port.path);
    })


    inquirer.prompt([{
        type: 'list',
        name: 'port',
        message: 'Selecciona el puerto',
        choices: listOfdevices
    },{
        type: 'input',
        name: 'farm',
        message: 'Escriba la camaronera'
    }])


        .then(answers => {
            openPortAndOutputData(answers.port, answers.farm);
        })

});


var message = {
    rssi: 0,
    qual: 0,
    apn: "null",
    imeiSim: "null",
    technology: 0,
    operator: "null",
    imeiHardware: "null",
    technologyOperator: 0,
    preferredTechnology: 0,
    bands: "null",
    lat: "null",
    lon: "null",
    date: "null",
    time: "null",
    farm: "null"
};

var callback = function(res){
    message["lat"] = (res["latitude"]);
    message["lon"] = (res["longitude"]);
};

function openPortAndOutputData(serialport, farm) {
    message['farm'] = farm.toUpperCase();
    var port = new SerialPort(serialport, {
        baudRate: 115200
    }, function (err) {
        if (err) {
            return console.log('Error: ', err.message)
        }
    });

    //option serial port
    const parser = port.pipe(new Delimiter({ delimiter: '\n' }));

    //open serial port
    port.on('open', function () {
        console.log('Opened Port.');
    });

    //read data serial port
    parser.on('data', function (data) {

        if (data.toString().includes('+CSQ:')) {
            const sennal = data.toString().substr(6).split(",");

            message['rssi'] = (113 - (parseInt(sennal[0], 10) * 2));
            message['qual'] = parseInt(sennal[1], 10);

            io.emit('sara:rssi', {
                value: message['rssi'].toString(),
                qual: message['qual'].toString()
            });
        }

        if (data.toString().includes('+URAT:')) {
            const command = data.toString().substr(7).split(",");
            message['technology'] = parseInt(command[0], 10);
            message['preferredTechnology'] = parseInt(command[1], 10);

            io.emit('sara:technology', {
                technology: message['technology'].toString(),
                preferredTechnology: message['preferredTechnology'].toString()
            });
        }

        if (data.toString().includes('+CGDCONT:')) {
            const command = data.toString().substr(10).split(",");
            message['apn'] = command[2].replace(/['"]+/g, '');

            io.emit('sara:apn', {
                apn: message['apn']
            });
        }

        if (data.toString().includes('+COPS:')) {
            const command = data.toString().substr(7).split(",");
            message['operator'] = command[2];
            message['technologyOperator'] = parseInt(command[3].replace('\r', ''), 10);

            io.emit('sara:operator', {
                operator: message['operator'],
                technologyOperator: message['technologyOperator']
            });
        }

        if (data.toString().includes('+CCID:')) {
            message['imeiSim'] = data.toString().substr(7).replace('\r', '');
            io.emit('sara:imeiSim', {
                imeiSim: message['imeiSim']
            });
        }

        if (data.toString().includes('+CGSN:')) {
            message['imeiHardware'] = data.toString().substr(7).replace('\r', '');
            io.emit('sara:imeiHardware', {
                imeiHardware: message['imeiHardware']
            });
        }

        if (data.toString().includes('+UBANDSEL:')) {
            message['bands'] = data.toString().substr(11).replace('\r', '');
            io.emit('sara:bands', {
                bands: message['bands']
            });
        }

        if (data.toString().includes('+ULOCAID:')) {
            // const command = data.toString().substr(10).split(",");
            // message['lat'] = command[3].replace(/['"]+/g, '');
            // message['lon'] = command[4].replace(/['"]+/g, '');
            io.emit('sara:location', {
                lat: message['lat'],
                lon: message['lon']
            });
        }



    });

    //error serial port
    port.on('err', function (data) {
        console.log(err.message);
    });

    // listen server
    server.listen(3000, () => {
        console.log('Server on port 3000');
        const ipWlan0 = ip.address();
        console.log('Browser: http://' + ipWlan0 + ':3000/');
        ipapi.location(callback);
        // console.log(message);      
    });

    const startTime = new Date(Date.now() + 5000);
    const cqs = schedule.scheduleJob({ start: startTime, rule: '*/1 * * * * *' }, function () {
        port.write('AT+CSQ\r');
    });
    const urat = schedule.scheduleJob({ start: startTime, rule: '*/5 * * * * *' }, function () {
        port.write('AT+URAT?\r');
    });
    const apn = schedule.scheduleJob({ start: startTime, rule: '*/10 * * * * *' }, function () {
        port.write('AT+CGDCONT?\r');
    });
    const operator = schedule.scheduleJob({ start: startTime, rule: '*/11 * * * * *' }, function () {
        port.write('AT+COPS?\r');
    });
    const imeiSim = schedule.scheduleJob({ start: startTime, rule: '*/12 * * * * *' }, function () {
        port.write('AT+CCID\r');
    });
    const imeiHardware = schedule.scheduleJob({ start: startTime, rule: '*/13 * * * * *' }, function () {
        port.write('AT+CGSN=1\r');
    });
    const bands = schedule.scheduleJob({ start: startTime, rule: '*/14 * * * * *' }, function () {
        port.write('AT+UBANDSEL?\r');
    });
    const location = schedule.scheduleJob({ start: startTime, rule: '*/15 * * * * *' }, function () {
        port.write('AT+ULOCAID?\r');
    });
};



client.on('connect', () => {
    const startTime = new Date(Date.now() + 10000);
    const push = schedule.scheduleJob({ start: startTime, rule: '*/60 * * * * *' }, function () {
        const now = new Date();
        message["date"] = date.format(now, 'YYYY/MM/DD');
        message["time"] = date.format(now, 'HH:mm:ss');

        if (message["imeiSim"] != "null"){
            client.publish('biofeeder/' + message["imeiSim"], JSON.stringify(message))
            // db.ref('devices/' + message["imeiSim"]).push(message);
            // console.log(message);
        }

    });
})


